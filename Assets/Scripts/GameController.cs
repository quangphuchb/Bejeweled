﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public Text text;
    public static float _score;
	// Use this for initialization
	void Start () {
        _score = 0;

    }
	
	// Update is called once per frame
	void Update () {
        text.text = "Score: " + _score.ToString();

    }
}
