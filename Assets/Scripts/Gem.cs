﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float x = transform.position.x;
        float y = transform.position.y;

        while (y > 0&& !Grid.gemAt(x, y - 1))
        {
            --y;
        }
        transform.position = new Vector2(x, y);

        Grid.solveMatches(Grid.matchesAt(x,y));
	}


    public bool sameType(Gem other)
    {
        return GetComponent<SpriteRenderer>().sprite == other.GetComponent<SpriteRenderer>().sprite;
    }
}
