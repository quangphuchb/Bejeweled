﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Select : MonoBehaviour
{
    float x = 0;
    float y = 0;
    public Transform selectRect;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //selectRect.position = new Vector2(x, y);

        Gem selected = Grid.gemAt(x, y);
        if (selectRect != null)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) && Grid.gemAt(x, y + 1))
                if (Input.GetKey(KeyCode.RightShift))
                {
                    if (validSwap(selected, Grid.gemAt(x, y + 1)))
                    {
                        swapAndSolve(selected, Grid.gemAt(x, y + 1));
                        ++y;
                    }
                }
                else
                    ++y;
            else if (Input.GetKeyDown(KeyCode.DownArrow) && Grid.gemAt(x, y - 1))
                if (Input.GetKey(KeyCode.RightShift))
                {
                    if (validSwap(selected, Grid.gemAt(x, y - 1)))
                    {
                        swapAndSolve(selected, Grid.gemAt(x, y - 1));
                        --y;
                    }
                }
                else
                    --y;
            else if (Input.GetKeyDown(KeyCode.RightArrow) && Grid.gemAt(x + 1, y))
                if (Input.GetKey(KeyCode.RightShift))
                {
                    if (validSwap(selected, Grid.gemAt(x + 1, y)))
                    {
                        swapAndSolve(selected, Grid.gemAt(x + 1, y));
                        ++x;
                    }
                }
                else
                    ++x;
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && Grid.gemAt(x - 1, y))
                if (Input.GetKey(KeyCode.RightShift))
                {
                    if (validSwap(selected, Grid.gemAt(x - 1, y)))
                    {
                        swapAndSolve(selected, Grid.gemAt(x - 1, y));
                        --x;
                    }
                }
                else
                    --x;
        }
        selectRect.position = new Vector2(x, y);
    }

    void swap(Gem a, Gem b)
    {
        Vector2 temp = a.transform.position;
        a.transform.position = b.transform.position;
        b.transform.position = temp;
    }

    bool validSwap(Gem a, Gem b)
    {
        swap(a, b);
        bool res = Grid.matchesAt(a.transform.position.x, a.transform.position.y).Count > 0 || Grid.matchesAt(b.transform.position.x, b.transform.position.y).Count > 0;
        swap(a, b);
        return res;
    }

    void swapAndSolve(Gem a, Gem b)
    {
        swap(a, b);
        Grid.solveMatches(Grid.matchesAt(a.transform.position.x, a.transform.position.y));
        Grid.solveMatches(Grid.matchesAt(b.transform.position.x, b.transform.position.y));
        GameController._score += 10;
    }
}
